<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class TrackingController extends Controller {

  public function eventos(Request $request){
    $dados = $_POST;

    if(empty($dados)){
      return response()->json(['Nenhum dado foi informado!'],500);
    }
    
    $eventos = json_decode($dados['events_json']);
    $visit_token = $dados['visit_token'];
    $visitor_token = $dados['visitor_token'];
    
    $eventos[0]->ip = $request->ip();
    $data = date('d/m/Y',$eventos[0]->time);
    $eventos = json_encode($eventos);

    $redis = app('redis');
    if(empty($redis)){
      return response()->json(['Não foi possivel gravar os dados!'],500);
    }else{
      $res = $redis->rpush("eventos:$data:$visitor_token", $eventos);
      return response()->json(['OK'],200);
    }
  }

}