<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class RelatorioController extends Controller {

  public function converter_data($data){
    if(strpos($data, '-') === false){
      $temp = explode('/',$data);
      return $temp[2] . '-' . $temp[1] . '-' . $temp[0] . ' 00:00:01';
    }else {
      return $data . ' 00:00:01';
    }
    
  }

  public function get_dados($data_ini, $data_fim){
    $redis = app('redis');

    if($data_ini == 'null' && $data_fim == 'null' || empty($data_ini) && empty($data_fim)){
      
      $keys = $redis->keys('*eventos*');
      $dados = [];
      foreach($keys as $key){
        $dados[str_replace('events:','',$key)] = $redis->lrange($key,0,-1);
      }
      
    }else{
      $begin = ($data_ini == 'null' || empty($data_ini))? date('Y-m-d H:i:s',strtotime('2020-01-01')) : date('Y-m-d H:i:s',strtotime($this->converter_data($data_ini)));
      $end = ($data_fim == 'null' || empty($data_fim))? date('Y-m-d H:i:s') : date('Y-m-d H:i:s',strtotime($this->converter_data($data_fim)));
   
      $dados = [];
      while($begin <= $end){

        $keys = $redis->keys(sprintf('eventos:%s:*',date('d/m/Y', strtotime($begin))));
        
        if(count($keys) > 0){
          $dados[date('d/m/Y', strtotime($begin))] = $redis->lrange($keys[0],0,-1);
        }
        $begin = date('Y-m-d H:i:s', strtotime($begin . ' +1 day'));
      }

    }
    return $dados;
  }

  public function relatorio1(Request $request){
    $parametros = $_POST;
    $dados = $this->get_dados(
	    isset($_POST["data_ini"])? $_POST["data_ini"]:null,
		isset($_POST["data_fim"])? $_POST["data_fim"]:null
    );
    $resultados = [];

    foreach($dados as $dado){
      foreach($dado as $d){
        $json = json_decode($d)[0];
        if(isset($json->properties->section)){
          if($json->properties->section == 'section'){
            continue;
          }

          if(!isset($resultados[$json->properties->section]) ){
            $resultados[$json->properties->section] = 0;
          }
          
          if($json->name == '$click'){
            $resultados[$json->properties->section]++;
          }
        }
      }
    }

    return $this->retornar_resultado($resultados);
  }

  public function relatorio2(Request $request){
    $parametros = $_POST;
   $dados = $this->get_dados(
	    isset($_POST["data_ini"])? $_POST["data_ini"]:null,
		isset($_POST["data_fim"])? $_POST["data_fim"]:null
    );

    $resultados = [];

    foreach($dados as $dado){
      foreach($dado as $d){
        $json = json_decode($d)[0];
        if (isset($json->properties->url)){
          if($json->properties->page == $parametros["pagina_produtos"] &&  $json->name == '$view'){
            //Elementos no HTML com o atributo 'id' foram configurados para ter o nome do anúncio no atributo 'data-id'
            $anuncio = $json->properties->view_id;
            
            if(!isset($resultados[$anuncio])){
              $resultados[$anuncio] = 0;
            }

            $resultados[$anuncio]++;

          }
        }
  
      }
    }

    return $this->retornar_resultado($resultados);
  }

  public function relatorio3(Request $request){
    $parametros = $_POST;
    $dados = $this->get_dados(
	    isset($_POST["data_ini"])? $_POST["data_ini"]:null,
		isset($_POST["data_fim"])? $_POST["data_fim"]:null
    );
    $resultados = [];

    $lastFilter = '';
    $filtroAntesPaginacao = '';
    foreach($dados as $dado){
      foreach($dado as $d){
        $json = json_decode($d)[0];
        if(isset($json->properties->section)){
          $lastFilter = $json->properties->section;      
        }
        if($lastFilter == 'section'){
          continue;
        }else if(!empty($lastFilter) && $parametros['paginacao'] != $lastFilter){
          $filtroAntesPaginacao = $lastFilter;
        }
  
        if($json->properties->page == $parametros["pagina_produtos"] &&  $json->name == '$view'){
          if(!isset($resultados[$lastFilter])){
            $resultados[$lastFilter] = 0;
          }
          $resultados[$lastFilter]++;

          if($parametros['paginacao'] == $lastFilter && !empty($filtroAntesPaginacao)){
            if(!isset($resultados[$filtroAntesPaginacao])){
              $resultados[$filtroAntesPaginacao] = 0;
            }
            $resultados[$filtroAntesPaginacao]++;
          }

        }
      }
    }

    return $this->retornar_resultado($resultados);
  }

  public function relatorio4(Request $request){
    $parametros = $_POST;
    $resultados = [];

    $dados = $this->get_dados(
	    isset($_POST["data_ini"])? $_POST["data_ini"]:null,
		isset($_POST["data_fim"])? $_POST["data_fim"]:null
    );

    $procura = null;
    $clicou = false;
    foreach($dados as $dado){
      foreach($dado as $d){
        $achou = false;
        $json = json_decode($d)[0];

        if(isset($json->properties->section)){
          if($json->properties->section == $parametros["campo_busca"] && $json->name = '$change'){
            
            if(isset($json->properties->value) && isset($json->properties->tag)){
              if($json->properties->tag == 'input'){
                $procura = $json->properties->value;

                if(!isset($resultados[$procura]) && !empty($procura)){
                  $resultados[$procura] = 0;
                }
                $resultados[$procura]++;
              }
            }
          }

        }
        
        if($json->name == '$click'){
          $clicou = true;
        }else if($json->name == '$view' && $clicou){
          $clicou = false;
          if($json->properties->page == $parametros['pagina_produtos']){
           
            if(isset($resultados[$procura]) && !empty($procura)){
             
              if($resultados[$procura] > 0){
                $resultados[$procura]--;
              }else{
                unset($resultados[$procura]);
              }
            }
            $procura = null;
          }
        }
      }
    }

    return $this->retornar_resultado($resultados, true);
  }

  public function relatorio5(Request $request){
    $parametros = $_POST;
    
    $resultados = [
      'A' => [],
      'B' => []
    ];
    $dados = $this->get_dados(null,'02/11/2020');

    $lastFilter = '';
    $filtroAntesPaginacao = '';
    foreach($dados as $dado){
      foreach($dado as $d){
        $json = json_decode($d)[0];
        if(isset($json->properties->section)){
          $lastFilter = $json->properties->section;      
        }
        if($lastFilter == 'section'){
          continue;
        }else if(!empty($lastFilter) && $parametros['paginacao'] != $lastFilter){
          $filtroAntesPaginacao = $lastFilter;
        }
  
        if($json->properties->page == $parametros["pagina_produtos"] &&  $json->name == '$view'){
          if(!isset($resultados['A'][$lastFilter])){
            $resultados['A'][$lastFilter] = 0;
          }
          $resultados['A'][$lastFilter]++;

          if($parametros['paginacao'] == $lastFilter && !empty($filtroAntesPaginacao)){
            if(!isset($resultados['A'][$filtroAntesPaginacao])){
              $resultados['A'][$filtroAntesPaginacao] = 0;
            }
            $resultados['A'][$filtroAntesPaginacao]++;
          }

        }
      }
    }
    
    $dados = $this->get_dados('02/11/2020',null);
    
    $lastFilter = '';
    $filtroAntesPaginacao = '';
    foreach($dados as $dado){
      foreach($dado as $d){
        $json = json_decode($d)[0];
        if(isset($json->properties->section)){
          $lastFilter = $json->properties->section;      
        }
        if($lastFilter == 'section'){
          continue;
        }else if(!empty($lastFilter) && $parametros['paginacao'] != $lastFilter){
          $filtroAntesPaginacao = $lastFilter;
        }
  
        if($json->properties->page == $parametros["pagina_produtos"] &&  $json->name == '$view'){
          if(!isset($resultados['B'][$lastFilter])){
            $resultados['B'][$lastFilter] = 0;
          }
          $resultados['B'][$lastFilter]++;

          if($parametros['paginacao'] == $lastFilter && !empty($filtroAntesPaginacao)){
            if(!isset($resultados['B'][$filtroAntesPaginacao])){
              $resultados['B'][$filtroAntesPaginacao] = 0;
            }
            $resultados['B'][$filtroAntesPaginacao]++;
          }

        }
      }
    }

    /* AJUSTANDO OS DADOS */
    if(empty($resultados)){
	    return [];  
	  }
    

	  arsort($resultados['A']);
	  arsort($resultados['B']);
    $res = [
      'A' => [], 'B' => []
    ];
 
    foreach($resultados['A'] as $key => $r){
      array_push($res['A'], ['informacao' => $key,'quantidade' => $r]);
    }
    
    foreach($resultados['B'] as $key => $r){
      if($key == null){
        continue;
      }
      
      array_push($res['B'], ['informacao' => $key,'quantidade' => $r]);
    }

    return response()->json($res,200);
  }

  public function retornar_resultado($res, $removerZerados = false){
    if(empty($res)){
	    return [];  
	  }
	  
    
	  arsort($res);
    $resultados = [];
 
    foreach($res as $key => $r){
      if($removerZerados && $r == 0){
        continue;
      }else{
        array_push($resultados, ['informacao' => $key,'quantidade' => $r]);
      }
    }

    return response()->json($resultados,200);
  }
  
}