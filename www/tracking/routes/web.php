<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('visitas', ['uses' => 'TrackingController@visitas']);

$router->post('eventos', ['uses' => 'TrackingController@eventos']);

$router->post('relatorios/1', ['uses' => 'RelatorioController@relatorio1']);

$router->post('relatorios/2', ['uses' => 'RelatorioController@relatorio2']);

$router->post('relatorios/3', ['uses' => 'RelatorioController@relatorio3']);

$router->post('relatorios/4', ['uses' => 'RelatorioController@relatorio4']);

$router->post('relatorios/5', ['uses' => 'RelatorioController@relatorio5']);