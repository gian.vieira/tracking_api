<?php

return [
  'redis' => [

    'cluster' => false,

    'default' => [
        'host'     => env('DB_HOST'),
        'port'     => env('DB_PORT'),
        'password' => env('DB_PASSWORD'),
        'database' => env('DB_DATABASE')
    ],

  ],
];