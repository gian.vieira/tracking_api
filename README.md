Sistema de WebTracking usando Redis

Este projeto foi construido como um Trabalho de Conclusão de Curso.
A biblioteca javascript de monitoramento utilizada é baseada na Ahoy.Js (https://github.com/ankane/ahoy.js)

Instalação

1. Instale docker e docker-compose no seu servidor (https://docs.docker.com/get-docker/)
2. Git Clone neste repositório
3. Dentro da pasta do repositório edite o arquivo .env para as portas do disponiveis no seu servidor (serão utilizadas pelo serviço do Apache) e configure uma senha para o Redis.
4. Dentro da pasta do repositório entre nas pastas subsequentes 'www/tracking' e configure o arquivo .env (fornecendo as mesmas portas do Apache e senha do Redis utilizadas acima).
5. Execute o docker através do comando 'docker-compose up -d --build' dentro da pasta do repositório.

Neste momento o serviço ja estará executando em seu servidor.

Utilização

1. Voce deve configurar o itens que devem ser monitorados pelo ferramenta.
Para realizar essa configuração, é necessário criar um arquivo javascript e confi-gurar a funçãoahoy.configure(). Os seguintes argumentos precisam ser configurados em formato objeto:

  **urlPrefix** - o IP e a porta do servidor onde os serviços Web de monitoramentoforam instalados;
  **trackingId** - o valor do atributoiddo elemento HTML que contém a descrição doproduto na página que o descreve;
  **trackingIdElem** - se a descrição do produto está no texto do elemento deve-se de-finir o valortext, se a descrição do produto está no valor de um atributo, especificaro nome desse atributo;
  **filtros** - o valor do atributodata-sectiondo elemento que engloba os botões denavegação e as categorias de produtos;
  **campo_busca** - o valor do atributodata-sectiondo elemento que contém os cam-pos de busca que devem ser monitorados;
  **carrossel** - o valor do atributodata-sectiondo elemento que contém o carrosselde produtos;
  **paginacao** - o valor do atributodata-sectiondo elemento que engloba a paginaçãodos resultados da busca;
  **pagina_produtos** - o nome da página que descreve um anúncio de produto se-lecionado pelo usuário.  É necessário que o site contenha uma página que exibedetalhes de um produto selecionado pelo usuário.

  Exemplo:

2. Após a instalação do serviço, a biblioteca de rastreamento fica disponível por meio do link "IP_INSTALADO":"PORTA_INSTALADA/tracking/public/js/ahoy.js".
  Este link de-verá ser referenciado em cada página do site que se deseja rastrear através da tag <script>.

3. No final da página que se deseja monitorar insera o seguinte script:
  <script>
    ahoy.configure({page: "nome_da_pagina"});
    ahoy.trackAll();
  </script>

Relatórios

Existe 4 relatórios construídos nesta ferramenta com a respectiva numeração:
  1. O relatório das ações dos usuários, exibe a quantidadede vezes que os usuários utilizaram o campo de busca, 
   os filtros, o carrossel e a paginação.Esse relatório não considera se a ação levou o usuário até um anúncio de seu interesse.
   A finalidade deste relatório é mostrar aos administradores quais as formas de navegação são mais procuradas pelos usuários do site;
  
  2.O relatório das ações produtivas realizadas pelos usuários, contabiliza a forma utilizada pelos usuários para chegar até os produtos de seu interesse.
  Considera-se que o produto era do interesse do usuário, se ele acessou a página detalhe desse produto. 
  Esse relatório contabiliza apenas a última ação realizada pelousuário para chegar na página detalhe do produto;
  
  3.O relatório dos produtos mais procurados, exibe osprodutos que os usuários possuem mais interesse. 
  Considera-se que o usuário teve inte-resse pelo produto, se ele abriu a página detalhe que descreve o produto.

  4. O relatório de buscas improdutivas, exibe as palavras-chave utilizadas em buscas que não retornaram resultados.
   Esse relatório é útil para queos administradores do site possam verificar quais são as dificuldades que os usuários 
   tem para encontrar um produto de seu interesse;

Para utilização dos relatórios basta chamar a função 'ahoy.relatorio(relatorio,data_inicial,data_final,function(dados))',
 passando os argumentos de:
  1. Numeração do relatório 
  2. Data Inicial dos registros (opcional)
  3. Data Final dos registros (opcional)
  4. Função callback
